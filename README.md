# cf-randhash

A Rust library containing randomized hashing algorithms.

# License

cf-randhash is distributed under the terms of either of the following
licenses

* [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)
* [MIT License](http://opensource.org/licenses/MIT)

at your option.

See [LICENSE-APACHE](LICENSE-APACHE) and [LICENSE-MIT](LICENSE-MIT)
for the full license texts.
