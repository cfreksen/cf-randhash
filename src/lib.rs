pub mod error;
pub mod hasher;
mod util;

pub fn xor(left: usize, right: usize) -> usize {
    left ^ right
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let res = xor(3, 5);
        assert_eq!(res, 6);
    }
}
