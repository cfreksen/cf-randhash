use thiserror::Error;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Error, Debug)]
#[error(transparent)]
pub struct Error(#[from] ErrorContent);

#[derive(Error, Debug)]
pub(crate) enum ErrorContent {
    #[error("seed size (at most {size_upper_bound}) is less than {required_size}")]
    InsufficientSeed {
        required_size: usize,
        size_upper_bound: usize,
    },
    #[error("invalid seed: {reason}")]
    InvalidSeed { reason: String },
}

pub(crate) fn err<T>(e: ErrorContent) -> Result<T> {
    Err(e.into())
}
