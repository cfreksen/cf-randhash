// From unstable feature "array_chunks": https://github.com/rust-lang/rust/issues/74985
#[inline]
pub(crate) fn as_chunks<T, const N: usize>(buf: &[T]) -> (&[[T; N]], &[T]) {
    assert_ne!(N, 0);
    let len = buf.len() / N;
    let (multiple_of_n, remainder) = buf.split_at(len * N);
    // SAFETY: We cast a slice of `len * N` elements into
    // a slice of `len` many `N` elements chunks.
    let array_slice: &[[T; N]] =
        unsafe { std::slice::from_raw_parts(multiple_of_n.as_ptr().cast(), len) };
    (array_slice, remainder)
}

// From the internal rust intrinsic of the same name
#[inline(always)]
pub(crate) unsafe fn transmute_unchecked<Src, Dst>(src: Src) -> Dst {
    // SAFETY: It's a transmute -- the caller promised it's fine.
    std::mem::transmute_copy::<Src, Dst>(&std::mem::ManuallyDrop::new(src))
}
