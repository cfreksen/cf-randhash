use crate::util::{as_chunks, transmute_unchecked};

pub trait FixedHasher<const N: usize, const M: usize> {
    fn hash(&self, a: &[u8; N]) -> [u8; M];
}

pub struct StdHasher<const N: usize, const M: usize, H: FixedHasher<N, M>> {
    inner: H,
    state: [u8; M],
}

impl<const N: usize, const M: usize, H: FixedHasher<N, M>> std::hash::Hasher
    for StdHasher<N, M, H>
{
    fn finish(&self) -> u64 {
        // SAFETY: Only performs unsafe actions to convert between N/M
        // and a constant after having checked that N/M is equal that
        // constant. Be wary of copy/paste errors!
        match M {
            4 => {
                let state = unsafe { transmute_unchecked::<[u8; M], [u8; 4]>(self.state) };
                u32::from_ne_bytes(state) as u64
            }
            8 => {
                let state = unsafe { transmute_unchecked::<[u8; M], [u8; 8]>(self.state) };
                u64::from_ne_bytes(state)
            }
            16 => {
                let state = unsafe { transmute_unchecked::<[u8; M], [u8; 16]>(self.state) };
                u128::from_ne_bytes(state) as u64
            }
            _ => {
                let mut buffer = [0u8; 8];
                let prefix = M.min(8);
                buffer[..prefix].copy_from_slice(&self.state[..prefix]);
                u64::from_ne_bytes(buffer)
            }
        }
    }

    fn write(&mut self, bytes: &[u8]) {
        let (chunks, remainder) = as_chunks::<u8, N>(bytes);
        // SAFETY: Only performs unsafe actions to convert between N/M
        // and a constant after having checked that N/M is equal that
        // constant. Be wary of copy/paste errors!
        match (N, M) {
            (4, 4) => {
                let mut state = unsafe { transmute_unchecked::<[u8; M], [u8; 4]>(self.state) };
                let chunks = unsafe { transmute_unchecked::<&[[u8; N]], &[[u8; 4]]>(chunks) };
                for chunk in chunks {
                    let hashee = u32::from_ne_bytes(state) ^ u32::from_ne_bytes(*chunk);
                    let hashee =
                        unsafe { transmute_unchecked::<[u8; 4], [u8; N]>(hashee.to_ne_bytes()) };
                    let hash = self.inner.hash(&hashee);
                    state = unsafe { transmute_unchecked::<[u8; M], [u8; 4]>(hash) };
                }
                self.state = unsafe { transmute_unchecked::<[u8; 4], [u8; M]>(state) };
            }
            (8, 8) => {
                let mut state = unsafe { transmute_unchecked::<[u8; M], [u8; 8]>(self.state) };
                let chunks = unsafe { transmute_unchecked::<&[[u8; N]], &[[u8; 8]]>(chunks) };
                for chunk in chunks {
                    let hashee = u64::from_ne_bytes(state) ^ u64::from_ne_bytes(*chunk);
                    let hashee =
                        unsafe { transmute_unchecked::<[u8; 8], [u8; N]>(hashee.to_ne_bytes()) };
                    let hash = self.inner.hash(&hashee);
                    state = unsafe { transmute_unchecked::<[u8; M], [u8; 8]>(hash) };
                }
                self.state = unsafe { transmute_unchecked::<[u8; 8], [u8; M]>(state) };
            }
            (8, 4) => {
                let mut state = unsafe { transmute_unchecked::<[u8; M], [u8; 4]>(self.state) };
                let chunks = unsafe { transmute_unchecked::<&[[u8; N]], &[[u8; 8]]>(chunks) };
                for chunk in chunks {
                    let hashee = u32::from_ne_bytes(state) as u64 ^ u64::from_ne_bytes(*chunk);
                    let hashee =
                        unsafe { transmute_unchecked::<[u8; 8], [u8; N]>(hashee.to_ne_bytes()) };
                    let hash = self.inner.hash(&hashee);
                    state = unsafe { transmute_unchecked::<[u8; M], [u8; 4]>(hash) };
                }
                self.state = unsafe { transmute_unchecked::<[u8; 4], [u8; M]>(state) };
            }
            (4, 8) => {
                let mut state = unsafe { transmute_unchecked::<[u8; M], [u8; 8]>(self.state) };
                let chunks = unsafe { transmute_unchecked::<&[[u8; N]], &[[u8; 4]]>(chunks) };
                for chunk in chunks {
                    let hashee = u64::from_ne_bytes(state) as u32 ^ u32::from_ne_bytes(*chunk);
                    let hashee =
                        unsafe { transmute_unchecked::<[u8; 4], [u8; N]>(hashee.to_ne_bytes()) };
                    let hash = self.inner.hash(&hashee);
                    state = unsafe { transmute_unchecked::<[u8; M], [u8; 8]>(hash) };
                }
                self.state = unsafe { transmute_unchecked::<[u8; 8], [u8; M]>(state) };
            }
            _ => {
                let mut buffer;
                for chunk in chunks {
                    buffer = *chunk;
                    for (buffer_byte, state_byte) in buffer.iter_mut().zip(&self.state) {
                        *buffer_byte ^= state_byte;
                    }
                    self.state = self.inner.hash(&buffer);
                }
            }
        }
        if !remainder.is_empty() {
            let mut buffer = [0u8; N];
            buffer[..remainder.len()].copy_from_slice(remainder);
            for (buffer_byte, state_byte) in buffer.iter_mut().zip(&self.state) {
                *buffer_byte ^= state_byte;
            }
            self.state = self.inner.hash(&buffer);
        }
    }
}
